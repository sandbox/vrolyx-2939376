<?php

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\twizo\Api\TwizoApi;

/**
 * Created by PhpStorm.
 * User: WesselVrolijks
 * Date: 15/01/2018
 * Time: 16:58
 */

class WidgetHandler {
    public function activateWidget(array &$form, FormStateInterface $form_state){
        // todo activate widget validation
        $twizo = new TwizoApi();
        $sessionToken = $twizo->createWidgetSession();
        $ajaxResponse = new AjaxResponse();
        $ajaxResponse->addCommand(new InvokeCommand(NULL, 'openWidget', [
            $sessionToken
        ]));

        return $ajaxResponse;
    }
}